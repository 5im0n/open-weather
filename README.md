# OpenWeather hybrid app

This project is develop with [Angular](https://angularjs.org/) and [Bootstrap](http://getbootstrap.com/)

## Requirements

Development tools:

  - [Node JS](http://nodejs.org/)
  - [Bower](http://bower.io/)
  - [Gulp](http://gulpjs.com/)


Install the dependencies:

  - `npm install`
  - `bower install`


## Publish the app

  - `gulp serve`


## Check the code with JSHint

  - `gulp check-code`


## Info

 The project follow the [AngularJS Style Guide](https://github.com/johnpapa/angularjs-styleguide)


## Versioning

This project is maintained under [the Semantic Versioning guidelines](http://semver.org/).

 - 0.0.1: Application structure
 - 0.1.0: Controller
 - 0.2.0: JSHint task
 - 0.3.0: Add style to the app
 - 0.4.0: Add routing
 - 0.5.0: Login and register form
 - 0.6.0: User service - (Login, logout)
 - 0.6.1: Password constraint directive
 - 0.6.2: Redirect the user if he is logged
 - 0.7.0: Weather service
 - 0.7.1: Weather icon directive
 - 0.7.2: Readme improvement
