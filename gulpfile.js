'use strict';

// ---------------------------------------------------------- //
// Dependencies
// ---------------------------------------------------------- //
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var gulp        = require('gulp');
var jshint      = require('gulp-jshint');
var less        = require('gulp-less');
var gutil       = require('gulp-util');
var rename      = require('gulp-rename');


var paths = {
  src: {
    scripts        : ['www/app/**/*.js'],
    templates      : ['www/index.html', 'www/app/**/*.html'],
    assets         : ['www/assets/fonts/**/*', 'www/assets/i18n/**/*', 'www/assets/img/**/*'],
    stylesFiles    : 'www/assets/styles/*.less',
    styleFile      : 'www/assets/styles/app.less',
    stylesFolder   : 'www/assets/styles/'
  },
  config: {
    jshintrc  : 'gulp/.jshintrc'
  }
};



// ---------------------------------------------------------- //
// Live reload server
// ---------------------------------------------------------- //
gulp.task('serve', ['build-styles', 'watch'], function() {

  browserSync({
    server: {
      baseDir : 'www/'
    },
    files          : [paths.src.templates, paths.src.scripts, paths.src.assets],
    port           : 8085,
    logConnections : true,
    logFileChanges : true
  });

});



// ---------------------------------------------------------- //
// Watch files
// ---------------------------------------------------------- //
gulp.task('watch', function() {
  gulp.watch(paths.src.stylesFiles, ['build-styles']);
});



// ---------------------------------------------------------- //
// Lint tasks
// ---------------------------------------------------------- //
gulp.task('check-code', function() {

  return gulp.src(paths.src.scripts)
    .pipe(jshint(paths.config.jshintrc))
    .pipe(jshint.reporter('jshint-stylish'));
});



// ---------------------------------------------------------- //
// Build styles (less)
// ---------------------------------------------------------- //
gulp.task('build-styles', function() {

  return gulp.src(paths.src.styleFile)
    .pipe(less())
    .on('error', gutil.log)
    .pipe(rename('build-dev.css'))
    .pipe(gulp.dest(paths.src.stylesFolder))
    .pipe(reload({ stream: true }));
});
