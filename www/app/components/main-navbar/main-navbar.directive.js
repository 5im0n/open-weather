(function() {

  'use strict';

  /** Main navbar directive
   */
  angular.module('app').directive('appMainNavbar', appMainNavbar);


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function appMainNavbar() {

    return {
      restrict    : 'E',
      templateUrl : 'app/components/main-navbar/main-navbar.html',
      controller  : MainNavbarController,
      controllerAs: 'mainNavbar'
    };

  }


  MainNavbarController.$inject = ['$window', 'userService', '$location'];

 // ----------------------------- //

  function MainNavbarController($window, userService, $location) {
    var vm = this;

    vm.goBack = goBack;
    vm.logout = logout;


    // ----------------------------- //


    // Back button action
    function goBack() {
      $window.history.back();
    }


    // Back button action
    function logout() {
      userService.logout();
      $location.path('/home');
    }

  }

})();
