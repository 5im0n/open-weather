(function() {

  'use strict';

  /** Application routes
   */
  angular.module('app').config(configRoute);

  configRoute.$inject = ['$routeProvider'];



  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function configRoute($routeProvider) {

    $routeProvider.
      when('/', {
        templateUrl  : 'app/routes/home/home.html'
      }).
      when('/register', {
        controller   : 'AccountController',
        controllerAs : 'register',
        templateUrl  : 'app/routes/account/register.html'
      }).
      when('/login', {
        controller   : 'AccountController',
        controllerAs : 'login',
        templateUrl  : 'app/routes/account/login.html'
      }).
      when('/weather-stations', {
        controller   : 'WeatherController',
        controllerAs : 'weather',
        templateUrl  : 'app/routes/weather/weather-stations.html'
      }).
      when('/weather-stations/:stationName', {
        controller   : 'WeatherController',
        controllerAs : 'weather',
        templateUrl  : 'app/routes/weather/weather-on-station.html'
      }).
      otherwise({
        redirectTo : '/'
      });


  }


})();
