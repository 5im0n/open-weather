(function() {

  'use strict';


  angular.module('app').factory('userService', userService);

  userService.$inject = ['$http', '$q', '$interpolate', 'localStorageService', '$CONSTANTS'];


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function userService($http ,$q, $interpolate, localStorageService, $CONSTANTS) {

    var user = {};

    return {
      getUser     : getUser,
      setUser     : setUser,
      login       : login,
      logout      : logout,
      saveToken   : saveToken,
      isLogged    : isLogged
    };


    // ----------------------------- //


    function getUser() {
      return user;
    }


    function setUser(u) {
      user = u;
    }


    function login(username, password) {

      var url = $interpolate($CONSTANTS.LOGIN_URL)({ username: username, password: password} );

      var deferred = $q.defer();

      $http.get(url)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function() {
          deferred.reject('Incorrect password');
        });

      return deferred.promise;

    }


    function logout() {
      localStorageService.remove('token');
    }


    function saveToken(token) {
      localStorageService.set('token', token);
    }


    function isLogged() {
      if (localStorageService.get('token') !== null) {
        return true;
      }
      else {
        return false;
      }
    }

  }

})();
