(function() {

  'use strict';

  /** Check password constraint
   */
  angular.module('app').directive('appPasswordConstraint', appPasswordConstraint);


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function appPasswordConstraint() {

    return {
      restrict : 'A',
      require  : 'ngModel',
      link     : function(scope, element, attrs, ngModel) {

        var forbiddenPassword = ['password', 'toto', '12345', 'azerty'];

        ngModel.$validators.passwordConstraint = function(modelValue) {
          var stat = true;

          for(var i = 0; i< forbiddenPassword.length; i++) {
            if(angular.equals(forbiddenPassword[i], modelValue)) {
              stat = false;
            }
          }

          return stat;

        };
      }

    };

  }

})();
