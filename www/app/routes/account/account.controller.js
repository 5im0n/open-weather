(function() {

  'use strict';

  /** Application controller
   */
  angular.module('app').controller('AccountController', AccountController);

  AccountController.$inject = ['userService', '$location'];


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function AccountController(userService, $location) {
    var vm = this;


    vm.user        = {};
    vm.formMessage = '';

    vm.login    = login;
    vm.register = register;


    // ----------------------------- //


    function login() {

      userService.login(vm.user.name, vm.user.password)
        .then(function(data) {
          userService.saveToken(data.token);
          userService.setUser(vm.user);
          $location.path('/weather-stations');
        }, function(msg){
          vm.formMessage = 'Incorrect password';
          console.log('Error ' + msg);
        });

    }


    function register() {
      console.log(vm.user);
    }


  }

})();
