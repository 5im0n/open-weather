(function() {

  'use strict';

  /** Application controller
   */
  angular.module('app').controller('AppController', AppController);

  AppController.$inject = ['$rootScope', 'userService'];


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function AppController($rootScope, userService) {
    var vm = this;


    vm.currentRoute = '';
    vm.isLogged     = isLogged;


    // When route change event
    $rootScope.$on('$routeChangeSuccess', function(event, currentRoute) {
      vm.currentRoute = currentRoute.originalPath;
    });


    function isLogged() {
      return userService.isLogged();
    }


  }

})();
