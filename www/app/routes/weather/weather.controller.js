(function() {

  'use strict';

  /** Application controller
   */
  angular.module('app').controller('WeatherController', WeatherController);

  WeatherController.$inject = ['$routeParams', 'weatherService'];


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function WeatherController($routeParams, weatherService) {
    var vm = this;


    vm.weatherService = weatherService;



    if(angular.isDefined($routeParams.stationName)) {

      weatherService.getWeatherOnStation($routeParams.stationName)
        .then(function(data) {
          vm.weather = data;
        }, function(msg){
          vm.weatherMessage = msg;
          console.log('Error ' + msg);
        });
    }


  }

})();
