(function() {

  'use strict';

  /** Capitalize a String
   */
  angular.module('app').filter('kelvinToCelsius', kelvinToCelsiusFilter);

  kelvinToCelsiusFilter.$inject = ['numberFilter'];


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function kelvinToCelsiusFilter(numberFilter) {

    var oneKelvin = 273.15;

    return function(kelvin) {

      return numberFilter(kelvin - oneKelvin, 1);

    };

  }

})();
