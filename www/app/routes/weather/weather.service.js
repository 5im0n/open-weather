(function() {

  'use strict';


  angular.module('app').service('weatherService', weatherService);

  weatherService.$inject = ['$http', '$q', '$interpolate', '$CONSTANTS'];


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function weatherService($http ,$q, $interpolate, $CONSTANTS) {

    this.getStations         = getStations;
    this.getWeatherOnStation = getWeatherOnStation;



    // ----------------------------- //



    function getStations() {

      return [
        {
          name: 'Cholet'
        },
        {
          name: 'Nantes'
        },
        {
          name: 'Rennes'
        },
        {
          name: 'Vannes'
        }

      ];
    }


    function getWeatherOnStation(stationName) {

      var url = $interpolate($CONSTANTS.GET_WEATHER_URL)({ station: stationName });

      var deferred = $q.defer();

      $http.get(url)
        .success(function(data) {
          if(angular.equals(data.cod, '404')) {
            deferred.reject('Unknow city');
          }
          else {
            deferred.resolve(data);
          }

        })
        .error(function(data) {
          deferred.reject('Error ' + data);
        });

      return deferred.promise;

    }


  }

})();
