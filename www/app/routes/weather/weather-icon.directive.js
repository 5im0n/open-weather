(function() {

  'use strict';

  /** Weather icon directive
   */
  angular.module('app').directive('weatherIcon', weatherIconDirective);


  // ---------------------------------------------------------- //
  // ---------------------------------------------------------- //


  function weatherIconDirective() {

    return {
      restrict    : 'E',
      replace     : true,
      templateUrl : 'app/routes/weather/weather-icon.html',
      scope       : {
        info: '@weatherMainInfo'
      },
      link        : link
    };


     // ----------------------------- //


    function link(scope, element, attrs) {

      var el = angular.element(element).children();

      scope.$watch(
        function () {
          return scope.info;
        },
        function(value) {

          scope.info = value;

          switch(scope.info) {
            case 'Rain':
              el.addClass('wi-rain');
              break;
            case 'Clouds':
              el.addClass('wi-cloudy');
              break;
            case 'Clear':
              el.addClass('wi-day-sunny');
              break;
            default:
              el.addClass('wi-cloudy');
          }

        }
      );


    }


  }

})();
