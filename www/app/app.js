(function() {

  'use strict';

  /** Application definition
   */
  angular.module('app', ['ngRoute', 'ngTouch', 'LocalStorageModule'])



  /** Application definition
   */
  .constant('$CONSTANTS', {
    LOGIN_URL       : 'https://customers-dev.mobiapps.fr/miniPortal/customers/formation/Backend-demo/' +
                      'Authentification/login.php?username={{username}}&password={{password}}',
    GET_WEATHER_URL : 'http://api.openweathermap.org/data/2.5/weather?q={{station}}'
  })


  /** Application Launch
   */
  .run(['$location', 'userService', function($location, userService) {

    // Reconnect the user if token exist
    if (userService.isLogged()) {
      $location.path('/weather-stations');
    }
    else {
      $location.path('/home');
    }

  }]);


})();
